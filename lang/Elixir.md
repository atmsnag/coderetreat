# Elixir

## Elixir はインストール済み

```
$ elixir -v
Erlang/OTP 18 [erts-7.3] [source] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false] [dtrace]

Elixir 1.2.3
```

## プロジェクトを作る

```
$ mix new example
$ cd example
```

## テストの書き方

ファイル名 : `test/example_test.exs`

```
defmodule ExampleTest do
  use ExUnit.Case
  doctest Example

  test "the truth" do
    assert Example.func1(10) == 11
  end
end
```

ファイル名: `lib/example.ex`

```
defmodule Example do
  def func1(x) do
    x + 1
  end
end
```

実行

```
$ mix test
```

## 後始末

```
$ cd ..
$ \rm -rf example
```

## LINK

* http://elixir-ja.sena-net.works/getting_started/1.html
* http://elixir-ja.sena-net.works/install.html - インストール
* https://elixirschool.com/jp/lessons/basics/testing/
