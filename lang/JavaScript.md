# JavaScript

## Node.js のインストール

* https://github.com/hokaccha/nodebrew

$HOME/.nodebrew 配下にディレクトリを作る

```
$ curl -L git.io/nodebrew | perl - setup
$ export PATH=$HOME/.nodebrew/current/bin:$PATH
$ nodebrew ls-remote
$ nodebrew install-binary v5.10.0
$ nodebrew use v5.10.0
$ rehash
$ which node
$HOME/.nodebrew/current/bin/node
$ ls ~/.nodebrew/node/
v5.10.0/
```

## package.json の作り方

```{.numberLines}
$ npm init --yes
$ npm install karma --save-dev
$ npm install jasmine --save-dev
$ cat package.json
{
  "name": "JavaScript",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "jasmine": "^2.4.1",
    "karma": "^0.13.22"
  }
}
```

## 準備

```
$ node_modules/karma/bin/karma init
files の質問には，'*.js' で答える - ロードするファイル名
$ egrep -v '//' karma.conf.js|uniq|grep -v '^$'
module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      '*.js'
    ],
    exclude: [
    ],
    preprocessors: {
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    concurrency: Infinity
  })
}
$ node_modules/karma/bin/karma start karma.conf.js
```

## テストの書き方

`a_spec.js` (テストするファイル)

```
describe("functions", function() {
  it("returns one", function() {
    expect(1).toBe(func1());
  });
});
```

`a.js` (テストされるファイル)

```
function func1() {
    return 1;
}
```

* describe()
    * 名前 - テストの見出し
    * テストブロック
* it()
    * 名前 - テストの名前
    * テストブロック
* expect()
    * 期待した値
    * 戻り値
* toBe(), toEqual()
    * Matcher のひとつ
    * toBe() - 同じオブジェクト
    * toEqual() - 同じ値

## テストの実行

自動的にブラウザが起動してテスト結果を表示するデーモンになる．

```
$ node_modules/karma/bin/karma start karma.conf.js
```

## 後始末

```
$ rm -rf ~/.nodebrew
$ rm -rf package.json karma.conf.js node_modules *.js
```

## LINK

* http://bps-tomoya.hateblo.jp/entry/2015/06/12/%E3%83%86%E3%82%B9%E3%83%88%E3%83%A9%E3%83%B3%E3%83%8A%E3%83%BC_karma_%2B_%E3%83%86%E3%82%B9%E3%83%88%E3%83%95%E3%83%AC%E3%83%BC%E3%83%A0%E3%83%AF%E3%83%BC%E3%82%AF_jasmine_%E3%81%AE%E7%92%B0%E5%A2%83
* http://qiita.com/shuhei/items/3e8b3a616225c5f30556

## メモ

```{.javascript .numberLines}
$ npm install foo -save      // for dependencies
$ npm install foo -save-dev  // for devDependencies
```
