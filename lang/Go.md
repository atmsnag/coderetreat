# Go

## Go

インストール済み

```
$ go version
go version go1.6 darwin/amd64
```

## テストの書き方

ファイル名: `func1_test.go`

```
package A

import (
    "testing"
)

func TestA(t *testing.T) {
    actual := Func1(10)
    expected := 11
    if actual != expected {
        t.Errorf("got %v\nwant %v", actual, expected)
    }
}
```

ファイル名: `func1.go`

```
package A

func Func1(i int) int {
    return 1 + i
}
```

実行

```
$ go test
PASS
ok  	_(ディレクトリ)	0.009s
```

## 後始末

```
$ \rm *.go
```


## LINK

* https://golang.org/dl/
* http://qiita.com/isaoshimizu/items/1a5d51aed98a57a9bcd4 - gvm (rbenv 的な)
* http://golang.org/pkg/testing/
* http://qiita.com/Jxck_/items/8717a5982547cfa54ebc - Assert が無いらしい…
