# PHP

## PHPUnit

### インストール

* https://phpunit.de/manual/current/ja/installation.html


```
$ wget https://phar.phpunit.de/phpunit.phar
  or
$ curl -LO https://phar.phpunit.de/phpunit.phar

$ mv phpunit.phar phpunit
$ chmod +x phpunit
$ ./phpunit --version
```

#### NOTE

PHP 5.3〜5.5 の人は，URL を `https://phar.phpunit.de/phpunit-old.phar` にすること．
サポートしている PHP のバージョンが違う．
詳しくは，`https://phpunit.de/` を参照．


### 書き方

* https://phpunit.de/manual/current/ja/writing-tests-for-phpunit.html

* `PHPUnit_Framework_TestCase` を継承する
* パブリックな `test*` 関数が実行される
* `$this->assertEquals()` などの関数で結果比較をする．

`ATest.php` - テストを実行するファイル

```
<?php
require "A.php";

class ATest extends PHPUnit_Framework_TestCase // を継承
{
    /* public な，test* 関数がテストになる */
    public function testAAA() {
        $a = new A();
        $expected = 11;
        $got = $a->func1(10);
        $this->assertEquals($expected, $got); // 前が期待した値, 後が取得した値
    }
}
```

`A.php` - テストされるファイル

```
<?php
class A {
    public function func1($x) {
        return $x + 1;
    }
}
```

### 実行方法

```
$ ./phpunit ATest
```

### 後始末

```
$ rm phpunit *.php
```
