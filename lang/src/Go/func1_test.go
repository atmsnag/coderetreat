package A

import (
    "testing"
)

func TestA(t *testing.T) {
    actual := Func1(10)
    expected := 11
    if actual != expected {
        t.Errorf("got %v\nwant %v", actual, expected)
    }
}
