require 'minitest/autorun'

require 'a'

class ATest < Minitest::Test
  def test_func1
    a = A.new
    got = a.func1(10)
    expected = 11
    assert_equal expected, got
  end
end
# vim: sw=2
