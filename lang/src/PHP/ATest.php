<?php
require "A.php";

class ATest extends PHPUnit_Framework_TestCase // を継承
{
    /* public な，test* 関数がテストになる */
    public function testAAA() {
        $a = new A();
        $expected = 11;
        $got = $a->func1(10);
        $this->assertEquals($expected, $got); // 前が期待した値, 後が取得した値
    }
}
