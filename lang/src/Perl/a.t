#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Test::More;

require_ok('A');

subtest 'sample' => sub {
    is(11, A::func1(10), 'func1');
};

done_testing;
