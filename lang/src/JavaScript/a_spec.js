describe('func1 Test', function() {
    it('return one', function() {
        var expected = 1;
        var got = func1();
        expect(expected).toBe(got);
    });
});

describe('func2 Test', function() {
    it('return recieved', function() {
        var expected = 1;
        var got = func2(1);
        expect(expected).toBe(got);
    });
});
