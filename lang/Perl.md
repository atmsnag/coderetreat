# Perl

## plenv / Perl のインストール

```
$ git clone git://github.com/tokuhirom/plenv.git ~/.plenv
$ git clone git://github.com/tokuhirom/Perl-Build.git ~/.plenv/plugins/perl-build/
```

以下のパス設定を，`.bashrc` や `.zshrc` に追加する．

```
export PATH="$HOME/.plenv/bin:$PATH"
eval "$(plenv init -)"
```

```
$ source ~/.zshrc
$ plenv install -l
$ plenv install 5.23.9
$ plenv global 5.23.9
$ perl -V|head -1
Summary of my perl5 (revision 5 version 23 subversion 9) configuration:
```

## テストの書き方

Perl の作法では，テストは，`.t` という拡張子を付けます．

ファイル名: `a.t`

```
#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

### -- ここまでお約束

use Test::More; # テスト用モジュール読み込み
use A; # テスト対象のクラスを読み込み

subtest 'sample' => sub { # テストのグルーピング
    is(11, A::func1(10), 'func1'); # A::func1() 関数のテスト
};

done_testing; # テスト終了の宣言
```

ファイル名: `A.pm`

```
package A;

use strict;
use warnings;
use utf8;

sub func1 {
    return $_[0] + 1;
}

1;
```

実行

```
$ prove a.t
```

## 後始末

```
$ \rm -rf ~/.plenv
```
