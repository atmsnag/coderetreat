# Ruby

## Ruby のインストール

### Prerequirements

* git
* Ruby がビルドできる環境(gcc, make, ...)

### Install

```
$ git clone https://github.com/rbenv/rbenv.git ~/.rbenv
$ git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
```

`export PATH="$HOME/.rbenv/bin:$PATH"` を .bashrc か .zshrc などに追加

```
$ source .zshrc or .bashrc
$ rehash
$ rbenv install -l
$ rbenv install 2.3.0
$ rbenv global 2.3.0
```

## テスティングフレームワーク

`minitest` がRuby には標準で含まれています．
Rails 4.2 辺りも標準でサポートしています．

## テストの書き方

ファイル名 : a_test.rb

```
require 'minitest/autorun'

require 'a'

class ATest < Minitest::Test
  def test_func1
    a = A.new
    got = a.func1(10)
    expected = 11
    assert_equal expected, got
  end
end
# vim: sw=2
```

ファイル名 : a.rb

```
class A
  def func1(x)
    x + 1
  end
end
# vim: sw=2
```

実行方法

```
$ ruby -I . a_test.rb
```

## 後始末

```
$ rm -rf `rbenv root`     // $HOME/.rbenv 配下が全部削除される
$ rm *.rb
```

## LINK

* http://railsguides.jp/testing.html - Rails 4.2 系のテストについて書いてある
* http://qiita.com/jnchito/items/ff4f7a23addbd8dbc460 - テスティングフレームワークのバージョンについて書いてある
    * http://allabout.co.jp/gm/gc/452071/ - その他
