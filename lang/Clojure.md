# Clojure

## Clojure のインストール

### Prerequirements

* Java

### Install

```
$ curl -LO https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
$ chmod +x lein
$ mkdir ~/.lein
$ vi ~/.lein/profiles.clj
$ cat ~/.lein/profiles.clj 
{:user {:local-repo "repo"}}
$ ./lein -v
Leiningen 2.6.1 on Java 1.8.0_31 Java HotSpot(TM) 64-Bit Server VM
```

## テストを書く

```
$ ./lein new app example
$ cd example
```

## テストの書き方

ファイル名 : `test/example/core_test.clj`

```
(ns example.core-test
  (:require [clojure.test :refer :all]
            [example.core :refer :all]))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= (func1 10) 11))))
```

ファイル名 : `src/example/core.clj`

```
(ns example.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn func1
  [x]
  (+ x 1))
```

実行方法

```
$ ../lein test
```

## 後始末

```
$ rm -rf ~/.lein
$ cd ..
$ rm -rf example lein
```

## LINK

* http://leiningen.org/
