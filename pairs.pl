#!/usr/bin/env perl

use strict;
use warnings;

my $debug = 0;
my @range;

if (@ARGV > 0) {
    my ($n) = @ARGV;
    @range = 1..$n;
} else {
    chomp(@range = <>);
}

my $pf = sub {
    printf("%s\t%s\n", sort {$a <=> $b} @_);
};

my $first;
while (@range > 0) {
    my $i = int rand scalar(@range);
    my $v = splice(@range, $i, 1);
    print $i, " ", $v, "\n" if ($debug);
    if (defined($first)) {
        $pf->($first, $v);
        $first = undef;
    } else {
        $first = $v;
    }
}

if (defined($first)) {
    $pf->($first, "?");
}
